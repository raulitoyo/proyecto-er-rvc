## Datos generales

UNIVERSIDAD ANDINA SIMON BOLIVAR
Maestría en Desarrollo Web

MAPEO OBJETO-RELACIONAL
Docente: Luis Eduardo Gonzáles Gonzáles

Alumno: Raúl Vargas Choquilla

## Descripción del proyecto

Se trata de realizar una simulación de un blog manejador de contenidos categorizable en multiples idiomas

Se debe crear:
- modelo, el objeto con el que se va a mapear la BD 
- migraciones es la estructura de la BD
- semilas, sistema laravel para introducir informacion falsa a la BD, sirven para mapear y testear los modelos que se realizaron 

## Procedimiento del trabajo realizado

Para realizar el presente trabajo se realizó los siguientes:

- Se ha revisado las instruciones del proyecto
- Se volvio a ver los videos de la clase para repasar algunas debilidades y falencias
- Se ha creado el repositorio en gitlab (proyecto_er_rvc)
- Se ha realizado un planificación mediante el uso de los issues, y se ha tratado de seguir lo planificado
- Se ha creado un archivo de contributing para seguir esa metodologia para el uso del git 
- Se ha creado el proyecto Laravel y se ha vinculado con el repositorio creado
- Se ha trabajado en ramas, según se indica en el contributing

## Procedimiento espec►fico del trabajo realizado

Especificamente se ha realizado los siguientes, según se indican en los respectivos commits:

- Se han creado los modelos
- Se han actualizado los modelos (restricciones y relaciones)
- Se han creado las migraciones
- Se han actualizado e insertado todos los campos del DER
- Se han creado las semillas
- Se han actualizado y configurado las semillas
- Se han creado los factories
- Se han actualizado y configurado los factories
