<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryLanguageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_language', function (Blueprint $table) {
            $table->integer('categories_id')->unsigned();
            $table->integer('languages_id')->unsigned();
            $table->string('label', 255);
            $table->string('slug', 255);
            $table->text('description');

            $table->primary(['categories_id', 'languages_id']);

            $table->foreign('categories_id')
                ->references('id')->on('categories')
                ->onDelete('cascade');

            $table->foreign('languages_id')
                ->references('id')->on('languages')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_language');
    }
}
