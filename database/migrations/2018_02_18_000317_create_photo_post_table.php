<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotoPostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photo_post', function (Blueprint $table) {
            $table->string('use', 255);
            $table->integer('order')->unsigned();
            $table->timestamps();
            $table->integer('photos_id')->unsigned();
            $table->integer('posts_id')->unsigned();

            $table->unique(['photos_id', 'posts_id']);

            $table->foreign('photos_id')
                ->references('id')->on('photos')
                ->onDelete('cascade');

            $table->foreign('posts_id')
                ->references('id')->on('posts')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photo_post');
    }
}
