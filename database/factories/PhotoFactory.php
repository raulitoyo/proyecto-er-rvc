<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Photo::class, function (Faker $faker) {
    return [
        'filename' => $faker->name,
        'type' => $faker->name,
    ];
});
