<?php

use Faker\Generator as Faker;

$factory->define(Model::class, function (Faker $faker) {
    return [
        'label' => $faker->name,
        'iso6391' => $faker->text(2),
    ];
});
