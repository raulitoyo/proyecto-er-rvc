<?php

use Faker\Generator as Faker;

$factory->define(Model::class, function (Faker $faker) {
    return [
		'label' => $faker->text(255),
		'slug' => $faker->text(255),
		'description' => $faker->text,
    ];
});
