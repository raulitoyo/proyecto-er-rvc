<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Publish::class, function (Faker $faker) {
    return [
        'slug' => $faker->name,
        'label' => $faker->name,
        'is_publish' => $faker->numberBetween(1, 5),
    ];
});
