<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);
         $this->call(PhotosTableSeeder::class);
         $this->call(PublishesTableSeeder::class);
         $this->call(CategoriesTableSeeder::class);
         $this->call(PostsTableSeeder::class);
         $this->call(PhotoPostTableSeeder::class);
         $this->call(CommentsTableSeeder::class);
         $this->call(LanguagesTableSeeder::class);
         $this->call(CategoryLanguageTableSeeder::class);
         $this->call(LanguagePostTableSeeder::class);
    }
}
