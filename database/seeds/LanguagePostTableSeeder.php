<?php

use Illuminate\Database\Seeder;

class LanguagePostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\LanguagePost::class, 300)->create();
    }
}
