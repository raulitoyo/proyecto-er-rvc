<?php

use Illuminate\Database\Seeder;

class PhotoPostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\PhotoPost::class, 50)->create();
    }
}
