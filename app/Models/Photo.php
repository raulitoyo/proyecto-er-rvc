<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $table = 'photos';

    protected $fillable = [
    	'filename',
    	'type'
    ];

    protected $hidden = [
    	'id',
    	'created_at',
    	'updated_at'
    ];

    public function photoposts()
    {
    	return $this->hasMany('App\Models\PhotoPost');
    }
}
