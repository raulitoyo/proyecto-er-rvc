<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryLanguage extends Model
{
    protected $table = 'category_language';

    protected $fillable = [
    	'label',
    	'slug',
    	'description'
    ];

    protected $hidden [
    	'categories_id',
    	'languages_id'
    ];

    public $timestamps = false;

    public function language()
    {
    	return $this->belongsTo('App\Models\Language');
    }

    public function category()
    {
    	return $this->belongsTo('App\Models\category');
    }
}
