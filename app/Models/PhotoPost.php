<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhotoPost extends Model
{
    protected $table = 'photo_post';

    protected $hidden = [
        'photos_id'
        'posts_id', 
        'created_at',
        'updated_at'
    ];

    public function post()
    {
    	return $this->belongsTo('App\Models\Post');
    }

    public function photo()
    {
    	return $this->belongsTo('App\Models\Photo');
    }
}
