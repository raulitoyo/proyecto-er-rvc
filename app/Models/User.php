<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'first_name', 
        'last_name', 
        'email', 
        'password',
        'remember_token'
    ];

    protected $hidden = [
        'id',
        'password', 
        'remember_token',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function comments()
    {
        return $this->hasMany('App\Models\Comment', 'autor_id');
    }

    public function posts()
    {
        return $this->hasMany('App\Models\Post', 'autor_id');
    }
}
