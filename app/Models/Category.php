<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    protected $hidden = [
    	'id',
    	'created_at',
    	'updated_at'
    ];

    public function categorylanguages()
    {
    	return $this->hasMany('App\Models\CategoryLanguage');
    }
}
