<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Publish extends Model
{
    protected $table = "publishes";

    protected $fillable = [
    	'slug',
    	'label',
    	'is_publish'
    ];

    protected $hidden = [
    	'id',
    	'created_at',
    	'updated_at'
    ];

    public function posts()
    {
    	return $this->hasMany('App\Models\Posts');
    }
}
