<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';

    protected $fillable = [
    	'comment'
    ];

    protected $hidden = [
    	'id',
    	'created_at',
    	'updated_at'
    ];

    public function user()
    {
    	return $this->belongsTo('App\Models\User');
    }

    public function post()
    {
    	return $this->belongsTo('App\Models\Post');
    }
}
