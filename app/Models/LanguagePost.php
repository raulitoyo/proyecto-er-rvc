<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LanguagePost extends Model
{
    protected $table = 'language_post';

    protected $fillable = [
    	'title',
    	'slug',
    	'content'
    ];

    protected $hidden = [
        'post_id'
        'languages_id'
    ];

    public $timestamps = false;

    public function post()
    {
    	return $this->belongsTo('App\Models\Post');
    }

    public function language()
    {
    	return $this->belongsTo('App\Models\Language');
    }
}
