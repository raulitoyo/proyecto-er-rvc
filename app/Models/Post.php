<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;

    protected $dates = [
    	'deleted_at',
    	'publish_at'
    ];

    protected $table = 'posts';

    protected $hidden = [
        'id',
        'publish_at',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function comments()
    {
    	return $this->hasMany('App\Models\Comment');
    }

    public function user()
    {
    	return $this->belongsTo('App\Models\User');
    }

    public function photoposts()
    {
    	return $this->hasMany('App\Models\PhotoPost');
    }

    public function publish()
    {
    	return $this->belongsTo('App\Models\Publish');
    }

    public function languageposts()
    {
    	return $this->hasMany('App\Models\LanguagePost');
    }
}
