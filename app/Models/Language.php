<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $table = 'languages';

    protected $fillable = [
    	'label',
    	'iso6391'
    ];

    protected $hidden = [
    	'id',
    	'created_at',
    	'updated_at'
    ];

    public function languageposts()
    {
    	return $this->hasMany('App\Models\LanguagePost');
    }

    public function categorialanguages()
    {
    	return $this->hasMany('App\Models\CategoryLanguage');
    }
}
